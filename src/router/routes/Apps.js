import { lazy } from 'react'

const AppRoutes = [
  {
    path: '/second-page',
    component: lazy(() => import('../../views/apps/SecondPage'))
  }
]

export default AppRoutes
