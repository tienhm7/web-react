import { Mail, Home } from 'react-feather'

export default [
  {
    id: 'dashboard',
    title: 'Home',
    icon: <Home size={20} />,
    navLink: '/dashboard'
  },
  {
    id: 'secondPage',
    title: 'Second Page',
    icon: <Mail size={20} />,
    navLink: '/second-page'
  }
]
